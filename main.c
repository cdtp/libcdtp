/*
 * This main file is just here for testing purposes and will eventually
 * get removed or moved to another location.
 *
 * Copyright (C) 2020  Matthias Heinz <mh@familie-heinz.name>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <bits/getopt_core.h>
#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/err.h>

#include "cdtp_main.h"


int send_username(uint8_t* username, unsigned int len, char** buffer_ptr, unsigned int* buffer_len) {
	/*
	 *
	 * Sending login credentials
	 *
	 */

	fprintf(stdout, "Sending login message\n");

	cdtp_data_msg_t msg;
	msg.msg = CDTP_CLI_LOGIN;
	msg.data = username;
	msg.len = len; // don't send the \0 from the string

	if(0 == cdtp_write_data_msg(&msg)) {
		return 1;
	}

	/*
	 *
	 * Reading answer to login credentials
	 *
	 */

	printf("Trying to read from buffer\n");

	char retbuf[sizeof(cdtp_msg_header_t)];
	int read = cdtp_read(retbuf, sizeof(cdtp_msg_header_t));
	if(read < 1) {
		fprintf(stderr, "Couldn't read a thing\n");
		return 1;
	}

	printf("Read exactly %i bytes.\n", read);

	if(read == sizeof(cdtp_msg_header_t)) {
		cdtp_msg_header_t *msg = (cdtp_msg_header_t*)retbuf;
		if(msg->msg == CDTP_LOGIN_ANS) {
			printf("Login OK!\n");
			*buffer_len = msg->len;

			if(msg->len > 0) {
				*buffer_ptr = malloc(msg->len);
				int read = cdtp_read(*buffer_ptr, msg->len);
				if(read != msg->len) {
					fprintf(stderr, "Error reading the login answer data.\n");
					return 1;
				}
			}
		}
		else if (msg->msg == CDTP_ERR) {
			printf("Got an error!\n");
			return 1;
		}
		else {
			fprintf(stderr, "Server sent only garbage.\n");
			return 1;
		}
	}
	else {
		fprintf(stderr, "Didn't understand what the server sent\n");
		return 1;
	}
	return 0;
}


int send_password(char * password, unsigned int len) {
	fprintf(stdout, "Sending password\n");

	cdtp_data_msg_t msg;
	msg.msg = CDTP_CLI_PW;
	msg.len = len;
	msg.data = (uint8_t*)password;

	if(0 == cdtp_write_data_msg(&msg)) {
		return 1;
	}

	char retbuf[1024];
	int read = cdtp_read(retbuf, sizeof(retbuf));
	if(read < 1) {
		fprintf(stderr, "Couldn't read a thing\n");
		return 1;
	}

	if(read == sizeof(cdtp_msg_header_t)) {
		cdtp_msg_header_t *msg = (cdtp_msg_header_t*)retbuf;
		if(msg->msg == CDTP_OK) {
			printf("Password OK!\n");
		}
		else if (msg->msg == CDTP_ERR) {
			printf("Got an error!\n");
			return 1;
		}
		else {
			fprintf(stderr, "Server sent only garbage.\n");
			return 1;
		}
	}
	else {
		fprintf(stderr, "Didn't understand what the server sent\n");
		return 1;
	}

	return 0;
}


int send_pub_key() {
	fprintf(stdout, "Storing the public key on the server\n");

	FILE *pub_key = fopen("public.pem", "rb");
	if(pub_key == NULL) {
		fprintf(stderr, "Error opening the public key file\n");
		return 1;
	}

	fseek(pub_key, 0, SEEK_END);
	long size = ftell(pub_key);
	rewind(pub_key);

	uint8_t buffer[size];

	size_t result = fread(buffer, 1, size, pub_key);
	if(result != size) {
		fprintf(stderr, "Error reading the public key file\n");
		return 1;
	}

	cdtp_data_msg_t msg;
	msg.msg = CDTP_CLI_SET_PUBKEY;
	msg.data = buffer;
	msg.len = size;

	if(0 == cdtp_write_data_msg(&msg)) {
		return 1;
	}

	/*
	 * Reading answer
	 */
	char retbuf[1024];
	int read = cdtp_read(retbuf, sizeof(retbuf));
	if(read < 1) {
		fprintf(stderr, "Couldn't read a thing\n");
		return 1;
	}

	if(read == sizeof(cdtp_msg_header_t)) {
		cdtp_msg_header_t *msg = (cdtp_msg_header_t*)retbuf;
		if(msg->msg == CDTP_OK) {
			printf("Successfully stored public key.\n");
		}
		else if (msg->msg == CDTP_ERR) {
			printf("Error storing the public key!\n");
		return 1;
		}
		else {
			fprintf(stderr, "Server sent only garbage.\n");
		return 1;
		}
	}
	else {
		fprintf(stderr, "Didn't understand what the server sent\n");
		return 1;
	}

	return 0;
}

void read_config_file(char * config_file, char ** username, char ** password) {
		// Config file should consist of one line: username:password
		FILE * cf = fopen(config_file, "r");
		if(cf == NULL) {
			fprintf(stderr, "Could not open config file.\n");
			exit(1);
		}

		char buffer[1024];
		if(fgets(buffer, sizeof(buffer), cf) == NULL) {
			fprintf(stderr, "Could not read config file.\n");
			exit(1);
		}

		char * cln_ptr = strchr(buffer, ':');
		if(cln_ptr == NULL) {
			fprintf(stderr, "No colon in first line of config file.\n");
			exit(1);
		}

		size_t cln = cln_ptr-buffer;
		*username = malloc(cln+1);
		memcpy(*username, buffer, cln+1);
		(*username)[cln] = '\0';

		*password = malloc(strlen(buffer) - cln - 1);
		memcpy(*password, buffer+cln + 1, strlen(buffer) - cln - 1);
		(*password)[strlen(buffer) - cln - 2] = '\0';
}

int check_if_exist_rsa_key() {

	int acc = access("priv.pem", F_OK | R_OK);
	if(acc == -1) {
		return 0;
	}

	acc = access("public.pem", F_OK | R_OK);
	if(acc == -1) {
		return 0;
	}

	return 1;
}

int gen_rsa_key() {
	RSA *rsa = RSA_new();

	unsigned long exp = RSA_F4;
	BIGNUM *b = BN_new();
	if(1 != BN_set_word(b, exp)) {
		return 1;
	}

	if(1 != RSA_generate_key_ex(rsa, 4096, b, NULL)) {
		return 1;
	}

	// save privat key
	BIO *priv_key = BIO_new_file("priv.pem", "w+");
	if(1 != PEM_write_bio_RSAPrivateKey(priv_key, rsa, NULL, NULL, 0, NULL, NULL)) {
		return 1;
	}
	BIO_free_all(priv_key);

	// save public key
	BIO *pub_key = BIO_new_file("public.pem", "w+");
	if(1 != PEM_write_bio_RSA_PUBKEY(pub_key, rsa)) {
		return 1;
	}
	BIO_free_all(pub_key);

	RSA_free(rsa);
	BN_free(b);

	return 0;
}

int decrypt_login_challenge(char * enc_data, unsigned int len, char * chlg) {
	RSA *rsa = NULL;
	BIO *priv_key = BIO_new(BIO_s_file());

	if(1 != BIO_read_filename(priv_key, "priv.pem")) {
		fprintf(stderr, "Error opening private key file.\n");
		return 0;
	}

	if(NULL == PEM_read_bio_RSAPrivateKey(priv_key, &rsa, NULL, NULL)) {
		fprintf(stderr, "Error reading private key file.\n");
		BIO_free_all(priv_key);
		return 0;
	}

	unsigned char buffer[RSA_size(rsa) - 42]; // TODO we expect 16 byte, don't we? What's safe here?

	if(-1 == RSA_private_decrypt(len, (const unsigned char*) enc_data, buffer, rsa, RSA_PKCS1_OAEP_PADDING)) {
		fprintf(stderr, "Error decrypting the challenge message.\n");

		while(ERR_peek_error()) {
			char err_buf[2048];
			ERR_error_string_n(ERR_get_error(), err_buf, sizeof(err_buf));
			fprintf(stderr, "%s\n", err_buf);
		}

		BIO_free_all(priv_key);
		RSA_free(rsa);
		return 0;
	}

	memcpy(chlg, buffer, 16);

	fprintf(stdout, "Received this challenge:\n");
	for(int i = 0; i < 16; i++) {
		fprintf(stdout, "0x%02x ", chlg[i]);
	}
	fprintf(stdout, "\n");

	RSA_free(rsa);
	BIO_free_all(priv_key);
	return 1;
}

int send_challenge(char * chlg) {
	cdtp_data_msg_t msg;
	msg.msg = CDTP_CLI_PUBKEY_CHLG_ANS;
	msg.len = 16;
	msg.data = (uint8_t*)chlg;

	if(0 == cdtp_write_data_msg(&msg)) {
		return 1;
	}

	char retbuf[1024];
	int read = cdtp_read(retbuf, sizeof(retbuf));
	if(read < sizeof(cdtp_msg_header_t)) {
		fprintf(stderr, "Couldn't read a thing\n");
		return 1;
	}

	if(read == sizeof(cdtp_msg_header_t)) {
		cdtp_msg_header_t *msg = (cdtp_msg_header_t*)retbuf;
		if(msg->msg == CDTP_OK) {
			printf("Challenge accepted!\n");
		}
		else if (msg->msg == CDTP_ERR) {
			printf("Got an error!\n");
		return 1;
		}
		else {
			fprintf(stderr, "Server sent only garbage.\n");
		return 1;
		}
	}
	else {
		fprintf(stderr, "Didn't understand what the server sent\n");
		return 1;
	}

	return 0;
}

int request_pubkey() {

	fprintf(stdout, "Please enter the email address for which the public key should be requested.\nEmail: ");

	char email[322]; // 320 chars for the email address, one for \n and one for \0

	// TODO rewrite this with fgets() so we can catch a \n as empty.
	int len = scanf("%s", email);

	// flush stdin
	int ch;
	while ((ch = getchar()) != '\n' && ch != EOF);

	fprintf(stdout, "\n");

	// TODO this is not working with scanf. Ignoring for now.
	if(len <= 0) {
		fprintf(stdout, "No email address entered. Returning.\n");
		return 1;
	}

	cdtp_data_msg_t msg;
	msg.msg = CDTP_CLI_REQ_PUBKEY;
	msg.len = strlen(email);
	msg.data = (uint8_t*)email;

	if(0 == cdtp_write_data_msg(&msg)) {
		fprintf(stderr, "Couldn't read a thing\n");
		return 0;
	}

	char retbuf[sizeof(cdtp_msg_header_t)];
	int read = cdtp_read(retbuf, sizeof(retbuf));
	if(read < sizeof(cdtp_msg_header_t)) {
		fprintf(stderr, "Couldn't read a thing\n");
		return 0;
	}

	if(read == sizeof(cdtp_msg_header_t)) {
		cdtp_msg_header_t *msg = (cdtp_msg_header_t*)retbuf;
		// handle received key
		if(msg->msg == CDTP_PUBKEY_ANS) {
			printf("Received a pubkey answer message!\n");

			char keybuf[msg->len];
			int read = cdtp_read(keybuf, msg->len);
			if(read < msg->len) {
				fprintf(stderr, "Didn't received the full public key message\n");
				return 0;
			}

			// TODO Store the fucking key \o/

			char pubkey_file_end[] = ".key";
			char pubkey_file_name[strlen(email) + sizeof(pubkey_file_end)];

			memcpy(pubkey_file_name, email, strlen(email));
			memcpy(pubkey_file_name + strlen(email), pubkey_file_end, sizeof(pubkey_file_end));

			FILE *pub_key = fopen(pubkey_file_name, "wb");
			if(pub_key == NULL) {
				fprintf(stderr, "Error opening the public key file for writing\n");
				return 1;
			}
			fwrite(keybuf, 1, msg->len, pub_key);
			fclose(pub_key);

		}
		else if (msg->msg == CDTP_ERR) {
			printf("Got an error!\n");
			return 0;
		}
		else {
			fprintf(stderr, "Server sent only garbage.\n");
			return 0;
		}
	}
	else {
		fprintf(stderr, "Didn't understand what the server sent\n");
		return 0;
	}


	return 1;
}


int send_email(char* from_addr) {

	// Part 1: Requesting the message
	char *to_addr = calloc(1,1);
	char buffer[10];

	fprintf(stdout, "Please enter the email address of the recipient.\nEmail: ");

	while (fgets(buffer, sizeof(buffer), stdin)) {
		to_addr = realloc(to_addr, strlen(to_addr) + 1 + strlen(buffer));
		strcat(to_addr, buffer);

		if(to_addr[0] == '\n') {
			fprintf(stdout, "No email address entered. Aborting.\n");
			return 0;
		}

		if(to_addr[strlen(to_addr)-1] == '\n') {
			// removing the trailing new line
			size_t len = strlen(to_addr);
			to_addr = realloc(to_addr, len-1);
			to_addr[len-1] = '\0';

			break;
		}
	}

	char *subject = calloc(1,1);
	fprintf(stdout, "Please enter the subject of the email.\nSubject: ");

	while (fgets(buffer, sizeof(buffer), stdin)) {
		subject = realloc(subject, strlen(subject) + 1 + strlen(buffer));
		strcat(subject, buffer);

		if(subject[0] == '\n') {
			fprintf(stdout, "No subject entered. Aborting.\n");
			return 0;
		}

		if(subject[strlen(subject)-1] == '\n') {
			// removing the trailing new line
			size_t len = strlen(subject);
			subject = realloc(subject, len-1);
			subject[len-1] = '\0';

			break;
		}
	}

	char *text = calloc(1,1);
	fprintf(stdout, "Please enter the text for the email. End with a newline and a dot (.).\nText:\n");

	while (fgets(buffer, sizeof(buffer), stdin)) {
		text = realloc(text, strlen(text) + 1 + strlen(buffer));
		strcat(text, buffer);

		if(text[0] == '\n') {
			fprintf(stdout, "No text entered. Aborting.\n");
			return 0;
		}

		if(text[strlen(text)-3] == '\n' && text[strlen(text)-2] == '.' && text[strlen(text)-1] == '\n') {
			// removing the dot and new lines
			size_t len = strlen(text);
			text = realloc(text, len-3);
			text[len-3] = '\0';

			break;
		}
	}

	// Part two formatting the message

	char msg_start[] = "<m>\n", msg_end[] = "</m>";
	char sbj_start[] = "<s>", sbj_end[] = "</s>\n";
	char pgf_start[] = "<p>", pgf_end[] = "</p>\n";

	char * email_txt = malloc(  strlen(msg_start)
						+ strlen(sbj_start) + strlen(subject) + strlen(sbj_end)
						+ strlen(pgf_start) + strlen(text)    + strlen(pgf_end)
						+ strlen(msg_end)
						+ 1 );

	// TODO handle newlines somehow. Replace them with a <br/>
	strcpy(email_txt, msg_start);
	strcpy(email_txt + strlen(email_txt), sbj_start);
	strcpy(email_txt + strlen(email_txt), subject);
	strcpy(email_txt + strlen(email_txt), sbj_end);
	strcpy(email_txt + strlen(email_txt), pgf_start);
	strcpy(email_txt + strlen(email_txt), text);
	strcpy(email_txt + strlen(email_txt), pgf_end);
	strcpy(email_txt + strlen(email_txt), msg_end);

	fprintf(stdout, "%s\n", email_txt);

	// TODO since this is plain text, we should add some kind of compression here to save bandwidth

	// TODO sign as well!

	// figuring out if there is a pubkey available for this emailaddress and encrypt the data with it
	char pubkey_file_end[] = ".key";
	char pubkey_file_name[strlen(to_addr) + sizeof(pubkey_file_end)];

	strcpy(pubkey_file_name, to_addr);
	strcpy(pubkey_file_name + strlen(to_addr), pubkey_file_end);

	if( access( pubkey_file_name, F_OK ) == -1 ) {
		fprintf(stderr, "File with name %s does not exist.\n", pubkey_file_name);
	}

	RSA *rsa = NULL;
	BIO *pub_key = BIO_new(BIO_s_file());



	if(1 != BIO_read_filename(pub_key, pubkey_file_name)) {
		fprintf(stderr, "Error opening public key file for %s.\n", to_addr);
		return 0;
	}

	if(NULL == PEM_read_bio_RSA_PUBKEY(pub_key, &rsa, NULL, NULL)) {
		fprintf(stderr, "Error reading public key file for %s.\n", to_addr);

		while(ERR_peek_error()) {
			char err_buf[2048];
			ERR_error_string_n(ERR_get_error(), err_buf, sizeof(err_buf));
			fprintf(stderr, "%s\n", err_buf);
		}

		BIO_free_all(pub_key);
		return 0;
	}

	unsigned char * encrptd_email = malloc(RSA_size(rsa));

	int encryptd_email_len = RSA_public_encrypt(strlen(email_txt) + 1, (const unsigned char*) email_txt, encrptd_email, rsa, RSA_PKCS1_OAEP_PADDING);
	if(-1 == encryptd_email_len) {
		fprintf(stderr, "Error encrypting the email.\n");

		while(ERR_peek_error()) {
			char err_buf[2048];
			ERR_error_string_n(ERR_get_error(), err_buf, sizeof(err_buf));
			fprintf(stderr, "%s\n", err_buf);
		}

		BIO_free_all(pub_key);
		RSA_free(rsa);
		return 0;
	}

	RSA_free(rsa);
	BIO_free_all(pub_key);

	// construct the message

	uint32_t msg_len = sizeof(cdtp_email_header_t)
							 + strlen(from_addr)
							 + strlen(to_addr)
							 + encryptd_email_len;
	char * email_msg = malloc(msg_len);

	cdtp_email_msg_t * email_msg_ptr = (cdtp_email_msg_t *) email_msg;

	email_msg_ptr->from_addr_len = strlen(from_addr);
	email_msg_ptr->to_addr_len = strlen(to_addr);
	email_msg_ptr->email_len = encryptd_email_len;

	memcpy((char*)(email_msg) + sizeof(cdtp_email_header_t), from_addr, strlen(from_addr));
	memcpy((char*)(email_msg) + sizeof(cdtp_email_header_t) + strlen(from_addr), to_addr, strlen(to_addr));
	memcpy((char*)(email_msg) + sizeof(cdtp_email_header_t) + strlen(from_addr) + strlen(to_addr), encrptd_email, encryptd_email_len);

	// now send the message
	cdtp_data_msg_t msg;
	msg.msg = CDTP_CLI_SEND_MAIL;
	msg.len = msg_len;
	msg.data = (uint8_t*)email_msg;

	if(0 == cdtp_write_data_msg(&msg)) {
		free(email_msg);
		return 1;
	}
	free(email_msg);

	char retbuf[1024];
	int read = cdtp_read(retbuf, sizeof(retbuf));
	if(read < 1) {
		fprintf(stderr, "Couldn't read a thing\n");
		return 0;
	}

	if(read == sizeof(cdtp_msg_header_t)) {
		cdtp_msg_header_t *msg = (cdtp_msg_header_t*)retbuf;
		if(msg->msg == CDTP_OK) {
			printf("Email sent successfully!\n");
		}
		else if (msg->msg == CDTP_ERR) {
			printf("Got an error!\n");
			return 0;
		}
		else {
			fprintf(stderr, "Server sent only garbage.\n");
			return 0;
		}
	}
	else {
		fprintf(stderr, "Didn't understand what the server sent\n");
		return 0;
	}

	return 1;
}


uint32_t look_for_mails() {
	uint32_t num_mails = 0;

	cdtp_msg_header_t msg;
	msg.msg = CDTP_CLI_LOOKUP_MAILS;
	msg.len = 0;

	if(0 == cdtp_write_msg(&msg)) {
		return 0;
	}


	char retbuf[sizeof(cdtp_msg_header_t)];
	int read = cdtp_read(retbuf, sizeof(cdtp_msg_header_t));
	if(read != sizeof(cdtp_msg_header_t)) {
		fprintf(stderr, "Error reading the header of server answer\n");
		return 0;
	}


	cdtp_msg_header_t * header_ptr = (cdtp_msg_header_t *) retbuf;


	if(header_ptr->msg == CDTP_CLI_LOOKUP_MAILS_ANS) {

		if(header_ptr->len == 0) {
			fprintf(stdout, "\nNo new mails on the server.\n");
			return 0;
		}

		char buffer[header_ptr->len];
		read = cdtp_read(buffer, header_ptr->len);

		if(read != header_ptr->len) {
			fprintf(stderr, "Didn't receive all expected data.\n");
			return 0;

		}
		fprintf(stdout, "\nFiles on the server:\n");

		uint32_t data_len = 0;
		while(data_len < header_ptr->len) {
			uint32_t filename_len = (uint32_t) buffer[data_len];

			if(filename_len > (header_ptr->len - data_len)) {
				fprintf(stderr, "The server is trying to mess with us. Please report this.\n");
				return 0;
			}
			num_mails++;

			data_len += sizeof(uint32_t);

			char filename[filename_len+1];
			memcpy(filename, buffer + data_len, filename_len);
			filename[filename_len] = '\0';
			data_len += filename_len;

			fprintf(stdout, "%s\n", filename);
		}
		fprintf(stdout, "\n");

	}
	else if (header_ptr->msg == CDTP_ERR) {
		printf("Got an error!\n");
		return 0;
	}
	else {
		fprintf(stderr, "Server sent only garbage.\n");
		return 0;
	}


	return num_mails;
}


void read_mail() {

	char *email_name = calloc(1,1);
	char buffer[10];

	fprintf(stdout, "Please enter the name of the mail to read.\nName: ");

	while (fgets(buffer, sizeof(buffer), stdin)) {
		email_name = realloc(email_name, strlen(email_name) + 1 + strlen(buffer));
		strcat(email_name, buffer);

		if(email_name[0] == '\n') {
			fprintf(stdout, "No name entered. Aborting.\n");
			return;
		}

		if(email_name[strlen(email_name)-1] == '\n') {
			// removing the trailing new line
			size_t len = strlen(email_name);
			email_name = realloc(email_name, len-1);
			email_name[len-1] = '\0';

			break;
		}
	}

	cdtp_data_msg_t msg;
	msg.msg = CDTP_CLI_FETCH_MAIL;
	msg.len = strlen(email_name);
	msg.data = (uint8_t*)email_name;

	int write_err = cdtp_write_data_msg(&msg);
	free(email_name);
	if(0 == write_err) {
		return;
	}


	char retbuf[sizeof(cdtp_msg_header_t)];
	int read = cdtp_read(retbuf, sizeof(cdtp_msg_header_t));
	if(read != sizeof(cdtp_msg_header_t)) {
		fprintf(stderr, "Error reading the header of server answer\n");
		return;
	}


	cdtp_msg_header_t * header_ptr = (cdtp_msg_header_t *) retbuf;

	if(header_ptr->msg == CDTP_CLI_FETCH_MAIL_ANS) {
		if(header_ptr->len == 0) {
			fprintf(stdout, "There is no mail with this name on the server.\n");
			return;
		}

		char buffer[header_ptr->len];
		read = cdtp_read(buffer, header_ptr->len);

		if(read != header_ptr->len) {
			fprintf(stderr, "Didn't receive all expected data.\n");
			return;

		}

		cdtp_email_header_t *email_hdr_ptr = (cdtp_email_header_t*)buffer;

		if(email_hdr_ptr->from_addr_len + email_hdr_ptr->to_addr_len + email_hdr_ptr->email_len
			+ sizeof(cdtp_email_header_t) != header_ptr->len) {
			fprintf(stderr, "The server is trying to screw with us. Report this.\n");
			return;
		}

		char from_addr[email_hdr_ptr->from_addr_len+1];
		char to_addr[email_hdr_ptr->to_addr_len+1];
		char email[email_hdr_ptr->email_len];

		memcpy(from_addr, buffer + sizeof(cdtp_email_header_t), email_hdr_ptr->from_addr_len);
		from_addr[email_hdr_ptr->from_addr_len] = '\0';

		memcpy(to_addr, buffer + sizeof(cdtp_email_header_t) + email_hdr_ptr->from_addr_len, email_hdr_ptr->to_addr_len);
		to_addr[email_hdr_ptr->to_addr_len] = '\0';

		memcpy(email, buffer + sizeof(cdtp_email_header_t) + email_hdr_ptr->from_addr_len + email_hdr_ptr->to_addr_len,  email_hdr_ptr->email_len);

		// decrypt the email
		RSA *rsa = NULL;
		BIO *priv_key = BIO_new(BIO_s_file());

		if(1 != BIO_read_filename(priv_key, "priv.pem")) {
			fprintf(stderr, "Error opening private key file.\n");
			return;
		}

		if(NULL == PEM_read_bio_RSAPrivateKey(priv_key, &rsa, NULL, NULL)) {
			fprintf(stderr, "Error reading private key file.\n");
			BIO_free_all(priv_key);
			return;
		}

		unsigned char decrypt_buffer[RSA_size(rsa) - 42];

		int decrypt_len = RSA_private_decrypt(email_hdr_ptr->email_len, (const unsigned char*) email, decrypt_buffer, rsa, RSA_PKCS1_OAEP_PADDING);

		if(-1 == decrypt_len) {
			fprintf(stderr, "Error decrypting email.\n");

			while(ERR_peek_error()) {
				char err_buf[2048];
				ERR_error_string_n(ERR_get_error(), err_buf, sizeof(err_buf));
				fprintf(stderr, "%s\n", err_buf);
			}

			BIO_free_all(priv_key);
			RSA_free(rsa);
			return;
		}

		RSA_free(rsa);
		BIO_free_all(priv_key);

		fprintf(stdout, "\nFrom: %s\n", from_addr);
		fprintf(stdout, "To: %s\n", to_addr);
		fprintf(stdout, "Message:\n%.*s\n\n", decrypt_len, decrypt_buffer);

	}
	else if (header_ptr->msg == CDTP_ERR) {
		printf("Got an error!\n");
		return;
	}
	else {
		fprintf(stderr, "Server sent only garbage.\n");
		return;
	}
}


typedef enum {
	STATE_LOGIN,
	STATE_PW_LGN,
	STATE_PUBKEY_LGN,
	STATE_PW_LGD_IN,
	STATE_CHK_PUBKEY,
	STATE_UPLD_PUBKEY,
	STATE_DECRPT_CHL,
	STATE_SEND_CHL,
	STATE_CHL_ACCPTED,
	STATE_RQST_PUBKEY,
	STATE_SEND_EMAIL,
	STATE_LOOK_FOR_EMAIL,
	STATE_READ_EMAIL,

} state_t;


char handle_state_msg(state_t state, void* opt_value) {
	char answer = '\0';
	switch(state) {
		case STATE_LOGIN:
			fprintf(stdout, "Connecting with username: %s.\n", (char *)opt_value);
			break;

		case STATE_PW_LGN:
			fprintf(stdout, "Provided password: %s.\nShall I procede with the login?\n(y/n) ", (char *)opt_value);
			scanf("%c", &answer);
			break;

		case STATE_PUBKEY_LGN:
			fprintf(stdout, "Login available by public key and by password.\nPlease choose how you want to login.\n");
			fprintf(stdout, " 1 Login by password\n");
			fprintf(stdout, " 2 Login by public key\n");
			fprintf(stdout, " q quit\n");
			fprintf(stdout, "(1-2): ");
			scanf("%c", &answer);
			fprintf(stdout, "\n");
			break;

		case STATE_PW_LGD_IN:
			fprintf(stdout, "Logged in via password. available options:\n");
			fprintf(stdout, " 1 Upload a public key\n");
			fprintf(stdout, " q quit\n");
			fprintf(stdout, "(1): ");
			scanf("%c", &answer);
			fprintf(stdout, "\n");
			break;

		case STATE_CHK_PUBKEY:
			fprintf(stdout, "Didn't find an existing RSA key.\nShould I create on? \n");
			fprintf(stdout, "(y/n): ");
			scanf("%c", &answer);
			fprintf(stdout, "\n");
			break;

		case STATE_UPLD_PUBKEY:
			fprintf(stdout, "Successfully uploaded the public key. Returning to available options.\n");
			break;

		case STATE_DECRPT_CHL:
			fprintf(stdout, "Decrypting the login challenge.\n");
			break;

		case STATE_SEND_CHL:
			fprintf(stdout, "Successfully decrypted challenge. Doing a login with it now\n");
			break;

		case STATE_CHL_ACCPTED:
			fprintf(stdout, "You are logged in.\nYour options are:\n");
			fprintf(stdout, " 1 Upload a public key (Will overwrite the current one) (Not working yet, because I'm too lazy)\n"); //TODO add option
			fprintf(stdout, " 2 Request a public key for an email address\n");
			fprintf(stdout, " 3 Send an email to an address (Note: This will fail if no public key is available)\n");
			fprintf(stdout, " 4 Look for new incoming mails\n");
			fprintf(stdout, " q quit\n");
			fprintf(stdout, "(1-4|q): ");
			scanf("%c", &answer);
			fprintf(stdout, "\n");
			break;

		case STATE_RQST_PUBKEY:
			fprintf(stdout, "Please enter the email address for which the public key should be requested:\n");
			break;

		case STATE_LOOK_FOR_EMAIL:
			fprintf(stdout, "You are have %i mails.\n", *((uint32_t *)opt_value));
			fprintf(stdout, "Your options are:\n");
			fprintf(stdout, " 1 Goto read mail menu\n");
			fprintf(stdout, " 1 Go back to the main menu\n");
			fprintf(stdout, " q quit\n");
			fprintf(stdout, "(1-2|q): ");
			scanf("%c", &answer);
			fprintf(stdout, "\n");

			break;

		case STATE_READ_EMAIL:
			fprintf(stdout, "Read another mail? (y/n) ");
			scanf("%c", &answer);
			fprintf(stdout, "\n");

			break;

		default:
			fprintf(stdout, "You didn't define a statement for this state!\n");
			break;
	}

	// flush stdin
	int ch;
	if(answer != '\0') while ((ch = getchar()) != '\n' && ch != EOF);

	return answer;
}


void print_usage() {
	fprintf(stderr, "Usage: libctpd -h hostname [-p port (default: 2020)] [-u username] [-w password] [-k private key location] [-c config file location]\n");
}

int main(int argc, char *argv[])
{
	int error = 0;
	char *hostname = NULL;
	char *port = NULL;
	char *username = NULL;
	char *password = NULL;
	char *key_loc = NULL;
	char *config_file = NULL;

	int opt;
	while ((opt = getopt(argc, argv, "h:p:u:w:k:c:")) != -1) {
		switch (opt) {
		case 'h':
			hostname = malloc(strlen(optarg));
			strcpy(hostname, optarg);
			break;
		case 'p':
			port = malloc(strlen(optarg));
			strcpy(port, optarg);
			break;
		case 'u':
			username = malloc(strlen(optarg));
			strcpy(username, optarg);
			break;
		case 'w':
			password = malloc(strlen(optarg));
			strcpy(password, optarg);
			break;
		case 'k':
			key_loc = malloc(strlen(optarg));
			strcpy(key_loc, optarg);
			break;
		case 'c':
			config_file = malloc(strlen(optarg));
			strcpy(config_file, optarg);
			break;
		default:
			print_usage();
			exit(1);
		}
	}

	if(hostname == NULL) {
		print_usage();
		exit(1);
	}

	if(port == NULL) {
		char default_port[] = "2020";
		port = malloc(strlen(default_port));
		strcpy(port, default_port);
	}

	if(config_file != NULL) {
		read_config_file(config_file, &username, &password);
	}

	if(username == NULL) {
		fprintf(stderr, "You either have to specify a config file or a username and a password.\n");
		exit(1);
	}

	if(password == NULL) {
			fprintf(stdout, "If you don't provide a password only public key login is possible.\n");
	}

	int err = cdtp_connect(hostname, port);

	if(err > 0) {
		return 1;
	}

	char chlg[16];
	char * chlg_buffer = NULL;
	unsigned int chlg_buffer_len;

	state_t state = STATE_LOGIN;
	while(1) {
		printf("State: %i\n", state);

		switch(state) {
			case STATE_LOGIN:
				handle_state_msg(state, username);
				error = send_username((uint8_t *)username, strlen(username), &chlg_buffer, &chlg_buffer_len);
				if(error) {
					err = 1;
					goto end;
				}

				if(chlg_buffer_len == 0) {
					fprintf(stdout, "Only password login available.\n");
					state = STATE_PW_LGN; // no data send, means no pubkey available, only password login possible
				}
				else {
					state = STATE_PUBKEY_LGN;
				}
				break;

			case STATE_PW_LGN:
				if(password == NULL) {
					fprintf(stdout, "You didn't provide a password by any means.\n");
					err = 1;
					goto end;
				}

				if('y' == handle_state_msg(state, password)) {
					error = send_password(password, strlen(password));
					if(error) {
						err = 1;
						goto end;
					}
					state = STATE_PW_LGD_IN;
				}
				else {
					err = 0;
					goto end;
				}

				break;

			case STATE_PUBKEY_LGN:
			{
				char answer = handle_state_msg(state, "");
				if(answer == '1') {
					state = STATE_PW_LGN;
				}
				else if(answer == '2') {
					state = STATE_DECRPT_CHL;
				}
				else if(answer == 'q') {
					err = 0;
					goto end;
				}
				break;
			}

			case STATE_PW_LGD_IN:
			{
				char answer = handle_state_msg(state, "");
				if(answer == '1') {
					state = STATE_CHK_PUBKEY;
				}
				else if(answer == 'q') {
					err = 0;
					goto end;
				}
				break;
			}

			case STATE_CHK_PUBKEY:
				if(check_if_exist_rsa_key()) {
					state = STATE_UPLD_PUBKEY;
				}
				else {
					char answer = handle_state_msg(state, "");
					if(answer == 'y') {
						if(gen_rsa_key()) {
							fprintf(stdout, "Error generating RSA key.\n");
							err = 1;
							goto end;
						}
						state = STATE_UPLD_PUBKEY;
					}
					else {
						fprintf(stdout, "No key to work with. Quitting.\n");
						err = 1;
						goto end;
					}
				}
				break;

			case STATE_UPLD_PUBKEY:
				error = send_pub_key();
				if(error) {
					err = 1;
					goto end;
				}
				handle_state_msg(state, "");
				state = STATE_PW_LGD_IN;
				break;

			case STATE_DECRPT_CHL:
				handle_state_msg(state, "");

				if(decrypt_login_challenge(chlg_buffer, chlg_buffer_len, chlg)) {
					state = STATE_SEND_CHL;
				}
				else {
					err = 1;
					goto end;
				}
				break;

			case STATE_SEND_CHL:
				handle_state_msg(state, "");
				if(send_challenge(chlg)) {
					err = 1;
					goto end;
				}
				state = STATE_CHL_ACCPTED;
				break;

			case STATE_CHL_ACCPTED:
			{
				char answer = handle_state_msg(state, "");
				if(answer == '1') {
					// TODO upload public key state
				}
				else if(answer == '2') {
					state = STATE_RQST_PUBKEY;
				}
				else if(answer == '3') {
					state = STATE_SEND_EMAIL;
				}
				else if(answer == '4') {
					state = STATE_LOOK_FOR_EMAIL;
				}
				else if(answer == 'q') {
					err = 0;
					goto end;
				}
				break;
			}

			case STATE_RQST_PUBKEY:
			{
				if(request_pubkey()) {
					fprintf(stdout, "Successfully saved the public key.\n");
				}
				else {
					fprintf(stdout, "There was an error retrieving the public key.\n");
				}
				state = STATE_CHL_ACCPTED;
				break;
			}

			case STATE_SEND_EMAIL:
			{
				send_email(username);
				state = STATE_CHL_ACCPTED;
				break;
			}

			case STATE_LOOK_FOR_EMAIL:
			{
				fprintf(stdout, "Looking for mails on the server.\n");
				uint32_t num_mails = look_for_mails();
				if(num_mails == 0) {
					state = STATE_CHL_ACCPTED;
					break;
				}

				char answer = handle_state_msg(state, (void*)&num_mails);
				if(answer == '1') {
					state = STATE_READ_EMAIL;
				}
				else if(answer == '2') {
					state = STATE_CHL_ACCPTED;
				}
				else if(answer == 'q') {
					err = 0;
					goto end;
				}
			}
				break;

			case STATE_READ_EMAIL:
			{
				read_mail();

				char answer = handle_state_msg(state, NULL);
				if(answer != 'y') {
					state = STATE_CHL_ACCPTED;
				}
			}
			break;

			default:
				err = 2;
				goto end;
				break;
		}
	}

end:
	cdtp_close();
	return err;
}
