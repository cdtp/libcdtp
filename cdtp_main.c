/*
 * CDTP main library file
 * Copyright (C) 2020  Matthias Heinz <mh@familie-heinz.name>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <openssl/ssl.h>
#include <openssl/bio.h>
#include <openssl/err.h>

#include <stdio.h>

#include "cdtp_main.h"

SSL *ssl;
SSL_CTX *ssl_ctx;
BIO *sbio, *bio_fp;


int cdtp_connect(char* hostname, char* port)
{
	ssl_ctx = SSL_CTX_new(TLS_client_method());
// 	SSL_CTX_set_verify(ssl_ctx, SSL_VERIFY_NONE, NULL);

	sbio = BIO_new_ssl_connect(ssl_ctx);
	BIO_get_ssl(sbio, &ssl);
	if (ssl == NULL) {
		// TODO print a good error message
		ERR_print_errors_fp(stderr);
		return 1;
	}

	SSL_CTX_set_mode(ssl_ctx, SSL_MODE_AUTO_RETRY);


	fprintf(stdout, "Setting hostname %s and port %s\n", hostname, port);
	BIO_set_conn_hostname(sbio, hostname);
  	BIO_set_conn_port(sbio, port);

	fprintf(stdout, "Attempting connect\n");

	int err = BIO_do_connect(sbio);
	if (err <= 0) {
		fprintf(stderr, "Could not connect to server\n");
		ERR_print_errors_fp(stderr);
		return 1;
	}

	fprintf(stdout, "Trying handshake\n");

	err = BIO_do_handshake(sbio);
	if (err != 1) {
		fprintf(stderr, "Handshake failed\n");
		return 1;
	}

	return 0;
}


void cdtp_close()
{
	BIO_free_all(sbio);
}

int cdtp_read(char* buff, int len)
{
     return BIO_read(sbio, buff, len);
}


int cdtp_write(char* buff, int len)
{
	return BIO_write(sbio, buff, len);
}

int cdtp_write_msg(cdtp_msg_header_t *msg)
{
	return cdtp_write((char*)msg, sizeof(cdtp_msg_header_t));
}

int cdtp_write_data_msg(cdtp_data_msg_t *msg)
{
	// let's start by sending the header
	if(0 == cdtp_write((char*)msg, sizeof(cdtp_msg_header_t))) {
		return 0;
	}

	char* ptr = (char*)msg->data;
	while(msg->len > INT_MAX) {
		if(0 == cdtp_write(ptr, INT_MAX)) {
			return 0;
		}

		msg->len -= INT_MAX;
		ptr += INT_MAX;
	}

	fprintf(stdout, "Sending %u bytes.\n", msg->len);
// 	for(int i = 0; i < msg->len; i++) {
// 		fprintf(stdout, "%c", msg->data[i]);
// 	}
// 	fprintf(stdout, "\n");


	if(msg->len > 0) {
		if(0 == cdtp_write(ptr, msg->len)) {
			return 0;
		}
	}

	return 1;
}
