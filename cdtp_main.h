/*
 * CDTP main header
 * Copyright (C) 2020  Matthias Heinz <mh@familie-heinz.name>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CDTP_MAIN_H
#define CDTP_MAIN_H

#include "../cdtp/protocol.h"

int cdtp_connect(char* hostname, char* port);
void cdtp_close();

// return 1 if data was successfully read and 0 otherwise.
int cdtp_read(char* buff, int len);

// return 1 if data was successfully written and 0 otherwise.
int cdtp_write_msg(cdtp_msg_header_t *msg);
int cdtp_write_data_msg(cdtp_data_msg_t *msg);


#endif //CDTP_MAIN_H
