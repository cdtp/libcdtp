cmake_minimum_required(VERSION 3.0)

project(libcdtp LANGUAGES C)


add_executable(libcdtp main.c cdtp_main.c)
TARGET_LINK_LIBRARIES(libcdtp ssl crypto)

install(TARGETS libcdtp RUNTIME DESTINATION bin)
